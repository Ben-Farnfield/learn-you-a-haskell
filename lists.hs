boomBang lst = [if x < 10 then "BOOM!" else "BANG!" | x <- lst, odd x]

fakeZip lstOne lstTwo = [(x, y) | x <- lstOne, y <- lstTwo]

length' lst = sum [1 | _ <- lst]

justUpperCase str = [x | x <- str, x `elem` ['A'..'Z']]
